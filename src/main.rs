use std::io::Read;
use std::fs::File;
use std::error::Error;
use std::path::{PathBuf, Path};

use clap::Parser;

use eqxbot::bot::{AppData, bot_main};
use eqxbot::settings::Settings;

#[derive(Parser)]
#[command(author, version, about, long_about=None)]
struct Args {
    /// Read settings from this file
    #[arg(short, long, value_name="FILE")]
    config: PathBuf,
}

fn read_settings(filename: &Path) -> Result<Settings, Box<dyn Error>> {
    let mut file = File::open(filename)?;
    let mut contents = String::new();

    file.read_to_string(&mut contents)?;

    Ok(toml::from_str(&contents)?)
}

fn main() -> Result<(), Box<dyn Error>> {
    let matches = Args::parse();
    let settings = read_settings(&matches.config)?;

    env_logger::builder()
        .filter(Some("eqxbot"), log::LevelFilter::Debug)
        .filter(Some("teloxide"), log::LevelFilter::Debug)
        .format_timestamp(None)
        .init();

    // web service
    bot_main(AppData {
        settings
    }).expect("failed to launch teloxide");

    Ok(())
}
