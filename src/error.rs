use std::io;
use std::path::PathBuf;

use thiserror::Error;

#[derive(Debug, Error)]
pub enum Error {
    #[error("Could not create tex file at {path:?}: {source}")]
    TexFile {
        #[source]
        source: io::Error,

        path: PathBuf,
    },

    #[error(transparent)]
    IO(#[from] io::Error),

    /// Most likely the pdflatex command is not available in PATH
    #[error("error running pdflatex: {0}")]
    PdflatexCommand(io::Error),

    /// The pdflatex program executed, but its exit code is non-zero
    #[error("pdflatex exited with status code: {status_code}\n\nstdout:\n\n{stdout}\n\nstderr:\n\n{stderr}")]
    Pdflatex {
        status_code: i32,
        stdout: String,
        stderr: String,
    },

    /// Most likely the convert command is not available in PATH
    #[error("error running convert: {0}")]
    ConvertCommand(io::Error),

    /// The convert program executed but its exit code is non-zero
    #[error("convert exited with status code: {status_code}\n\nstdout:\n\n{stdout}\n\nstderr:\n\n{stderr}")]
    Convert {
        status_code: i32,
        stdout: String,
        stderr: String,
    },
}

impl Error {
    pub fn to_emoji(&self) -> String {
        match self {
            Error::Pdflatex { .. } => "Invalid tex 🥴".to_string(),
            _ => "Server Error 😭".into(),
        }
    }
}
