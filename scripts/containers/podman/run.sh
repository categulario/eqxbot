#!/bin/bash

__dir=$(dirname $(realpath $0))

container=eqxbot
image=registry.gitlab.com/categulario/eqxbot/eqxbot

if [[ -z $@ ]]; then
    cmd=$image
    name="--name=$container"

    podman rm -i $container
else
    cmd="-it $image $@"
fi

podman run --rm \
    --uidmap 900:0:1 --uidmap 0:1:900 \
    --volume $__dir/../../../settings.toml:/etc/eqxbot/settings.toml:U \
    $name \
    $cmd
