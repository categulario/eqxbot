#[macro_use] extern crate serde_derive;

mod latex;
mod error;
pub mod bot;
pub mod settings;
pub mod hashing;
