use actix_web::{get, post, web, App, HttpResponse, HttpServer, Responder};

use crate::telegram::Update;
use crate::common::{AppState, ImageRequest};

#[derive(Serialize, Deserialize)]
struct Msg {
    msg: String,
}

#[post("/bot/{token}")]
async fn bot_index(token: web::Path<String>, update: web::Json<Update>, app_state: web::Data<AppState>) -> impl Responder {
    if token.into_inner() != app_state.settings.token {
        return HttpResponse::Ok().json(Msg {
            msg: "Invalid token".into(),
        });
    }

    let request = if update.0.is_message() {
        let message = update.0.get_message();

        ImageRequest::TextMessage{
            text: message.text.clone(),
            chat_id: message.chat.id,
            message_id: message.message_id,
        }
    } else {
        let query = update.0.get_inline_query();

        ImageRequest::InlineQuery{
            text: query.query.clone(),
            query_id: query.id.clone(),
        }
    };

    match app_state.transmitter.send(request) {
        Ok(_) => {
            HttpResponse::Ok().json(Msg {
                msg: "Request sent to worker thread".into(),
            })
        },
        Err(e) => {
            log::error!("Worker thread dead: {}", e);

            HttpResponse::Ok().json(Msg {
                msg: format!("Worker thread probably dead: {}", e),
            })
        },
    }
}

#[get("/")]
async fn hello() -> impl Responder {
    HttpResponse::Ok().json(Msg {
        msg: "Hola, mundo!".into(),
    })
}

#[actix_web::main]
pub async fn api_main(app_state: AppState) -> std::io::Result<()> {
    let app_config = app_state.clone();

    HttpServer::new(move || {
        App::new()
            .data(app_config.clone())
            .service(hello)
            .service(bot_index)
    })
    .bind(&format!("{}:{}", app_state.settings.host, app_state.settings.port))?
    .run()
    .await
}
