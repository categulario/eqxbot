use std::error::Error;

use teloxide::prelude::*;
use teloxide::types::InputFile;

use crate::settings::Settings;
use crate::latex::render;

pub struct AppData {
    pub settings: Settings,
}

async fn message_handler(bot: Bot, message: Message, data: &'static AppData) -> Result<(), ()> {
    if let Some(text) = message.text() {
        match render(text, &data.settings) {
            Ok(r) => {
                bot
                    .send_photo(message.chat.id, InputFile::file(r.img_path))
                    .await.unwrap();
            }
            Err(e) => {
                log::error!("Error during message query: {e}");

                bot
                    .send_message(message.chat.id, e.to_emoji())
                    .await.unwrap();
            }
        }
    } else {
        bot.send_message(message.chat.id, "Unsupported message").await.unwrap();
    }

    Ok(())
}

async fn run(data: &'static AppData) -> Result<(), Box<dyn Error>> {
    log::info!("Starting bot...");

    let bot = Bot::new(&data.settings.token);

    let handler = dptree::entry()
        .branch(Update::filter_message().endpoint(message_handler));

    Dispatcher::builder(bot, handler)
        .dependencies(dptree::deps![data])
        .enable_ctrlc_handler()
        .build()
        .dispatch()
        .await;

    Ok(())
}

#[tokio::main]
pub async fn bot_main(data: AppData) -> Result<(), Box<dyn Error>> {
    let static_ref: &'static _ = Box::leak(Box::new(data));

    run(static_ref).await
}
