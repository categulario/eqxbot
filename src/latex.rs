use std::fs::File;
use std::io::Write;
use std::process::Command;
use std::path::PathBuf;

use crate::error::Error;
use crate::settings::Settings;
use crate::hashing::hash;

pub struct RenderResponse {
    pub img_path: PathBuf,
    pub hash: String,
}

pub fn render(query: &str, settings: &Settings) -> Result<RenderResponse, Error> {
    let Settings { tmp_dir, .. } = settings;
    let texthash = hash(query);

    let texfilename = format!("{}.tex", texthash);
    let pdffilename = format!("{}.pdf", texthash);
    let imgfilename = format!("{}.jpeg", texthash);

    let texfilepath = tmp_dir.join(texfilename);
    let pdffilepath = tmp_dir.join(pdffilename);
    let imgfilepath = tmp_dir.join(imgfilename);

    if imgfilepath.exists() {
        log::debug!("Using cached image for hash: {}", texthash);

        return Ok(RenderResponse {
            img_path: imgfilepath,
            hash: texthash,
        });
    }

    let mut texfile = File::create(&texfilepath)
        .map_err(|e| Error::TexFile {
            source: e,
            path: texfilepath.clone(),
        })?;

    write!(texfile, "\\nonstopmode
\\documentclass{{minimal}}
\\usepackage{{amsmath}}
\\usepackage{{amssymb}}
\\usepackage[active,tightpage]{{preview}}
\\usepackage{{transparent}}
\\begin{{document}}
\\begin{{preview}}
\\begin{{math}}\\displaystyle\\arraycolsep=2pt\\def\\arraystretch{{1}}
{}
\\end{{math}}
\\end{{preview}}
\\end{{document}}\n", query).map_err(|e| Error::TexFile {
    source: e,
    path: texfilepath.clone(),
})?;

    let latex_output = Command::new("pdflatex")
        .arg(format!("-output-directory={}", tmp_dir.to_str().unwrap()))
        .arg("-interaction=nonstopmode")
        .arg(&texfilepath)
        .output()
        .map_err(Error::PdflatexCommand)?;

    if !latex_output.status.success() {
        let status_code = latex_output.status.code().unwrap();
        let stdout = String::from_utf8_lossy(&latex_output.stdout).into_owned();
        let stderr = String::from_utf8_lossy(&latex_output.stderr).into_owned();
        log::error!("Failed pdflatex command with exit code: {status_code}\n\nStdout:\n\n{stdout}\n\nStderr:\n\n{stderr}");

        return Err(Error::Pdflatex {
            status_code, stdout, stderr,
        });
    }

    let output = Command::new("convert")
        .arg("-density").arg("300")
        .arg("-quality").arg("100")
        .arg("-flatten")
        .arg(format!("pdf:{}", pdffilepath.to_str().unwrap()))
        .arg(format!("jpeg:{}", imgfilepath.to_str().unwrap()))
        .output().map_err(Error::ConvertCommand)?;

    if !output.status.success() {
        let status_code = output.status.code().unwrap();
        let stdout = String::from_utf8_lossy(&output.stdout).into_owned();
        let stderr = String::from_utf8_lossy(&output.stderr).into_owned();
        log::error!("Failed convert command with exit code: {status_code}\n\nStdout:\n\n{stdout}\n\nStderr:\n\n{stderr}");

        return Err(Error::Convert {
            status_code, stdout, stderr,
        });
    }

    log::debug!("Sucessfully rendered image for hash {}", texthash);

    Ok(RenderResponse {
        img_path: imgfilepath,
        hash: texthash,
    })
}
