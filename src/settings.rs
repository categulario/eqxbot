use std::path::PathBuf;

fn default_tmp_dir() -> PathBuf { "/tmp".into() }

#[derive(Clone, Debug, Deserialize)]
pub struct Settings {
    /// Where to store temporary files used by this service.
    #[serde(default = "default_tmp_dir")]
    pub tmp_dir: PathBuf,

    /// A unique and secret string used to identified valid clients.
    pub token: String,
}
