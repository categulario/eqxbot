use sha3::{Digest, Sha3_256};

pub fn hash(s: &str) -> String {
    // create a Sha1 object
    let mut hasher = Sha3_256::new();

    // write input message
    hasher.update(s.as_bytes());

    let result = hasher.finalize();

    format!("{:x}", result)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn check_digest() {
        assert_eq!(hash("pollo"), "561a64cf4df0721fab31cc638ea52607229598a416040764b4ab032f86103481");
    }
}
