FROM docker.io/rust:1.67-bookworm AS build

COPY ./Cargo.toml /
COPY ./Cargo.lock /
COPY ./src /src

RUN cargo build --locked --release

FROM debian:bookworm

# latex related packages are needed for latex dendering.
# ca-certificates is needed for teloxide to make the requests to the telegram
# api.
# imagemagick and gs (ghostscript) are used to convert pdflatex's pdf output to
# jpg.
RUN apt-get update && apt-get install -y --no-install-recommends \
    texlive \
    preview-latex-style \
    texlive-latex-extra \
    ca-certificates \
    imagemagick \
    ghostscript \
    && rm -rf /var/lib/apt/lists/*

# apply imagemagick policy that allows reading ghostscript and pdf. See README
COPY ./scripts/containers/imagemagick/policy.xml /etc/ImageMagick-6/

RUN useradd --system --create-home --user-group \
    --uid 900 --home-dir /usr/lib/eqxbot \
    --shell /bin/false \
    eqxbot

COPY --from=build /target/release/eqxbot /usr/bin/eqxbot

USER eqxbot

ENTRYPOINT ["/usr/bin/eqxbot"]

CMD ["-c", "/etc/eqxbot/settings.toml"]
