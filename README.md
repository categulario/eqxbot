# EqxBot

El bot telegram que transforma código latex en ecuaciones

## Requisitos

* comando `pdflatex` de texlive por ejemplo. En archlinux se necesita
  `texlive-core`.
* ImageMagick
* permiso `read` para pdfs y ghostscript en imagemagick (ver
  `/etc/ImageMagick-7/policy.xml`)

    <policy domain="coder" rights="read" pattern="GS" />
    <policy domain="coder" rights="read" pattern="PDF" />

* paquete `preview` de latex (`texlive-latexextra` en archlinux)
  (`preview-latex-style` en ubuntu) y `transparent.sty` (`texlive-latex-extra en
  ubuntu)

## Desarrollo

Para probar la API en local necesitarás un archivo `settings.toml` con al menos
estas variables:

```toml
token = "123456"
```

Y puedes correr el bot con:

    git clone https://github.com/categulario/eqxbot.git
    cd eqxbot
    cargo run -- --config settings.toml

Adicionalmente puedes pasar la variable de entorno `RUST_LOG` para tener más
información sobre los sucesos.

    RUST_LOG=eqxbot=debug,actix_server=info cargo run -- --config settings.toml
